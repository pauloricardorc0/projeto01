import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    containerA: {
        flex: 1,
        alignItems: "center",
    },
    containerB: {
        width: "100%",
        height: 200,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20
    },
    texto: {
        fontSize: 17,
        marginBottom: 7,
    },
    botao:{
        borderWidth: 2,
        borderColor:"#DDD",
        width: 120,
        alignItems: "center",
        padding: 10,
        borderRadius: 20,
        backgroundColor: "#FFF",
        margin: 20,
    },
    textobotao:{
        fontSize: 20,
    },
    botaoParticipar:{
        borderRadius: 7,
        borderWidth: 2,
        borderColor:"#DDD",
        width: 150,
        alignItems: "center",
        padding: 10,
        backgroundColor: "#FFF",
        margin: 20,
    }


});