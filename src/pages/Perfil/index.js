import React from 'react';
import { View, Image, ScrollView, TouchableOpacity } from 'react-native';

import styles from './styles';

export default function Perfil() {
  return (
    <View style={styles.container}>
        <ScrollView>
            <Image/>

            <View>
                <View> 
                    <Text>Primeiro Nome:</Text>
                    <Text>Segundo Nome:</Text>
                </View>
                <View>
                    <Text>João</Text>
                    <Text>Elias</Text>
                </View>
            </View>

            <Text>E-mail:</Text>
            <Text>joao@gmail.com</Text>

            <Text>Direito de Acesso e Correção (Artigo 18, II, III - LGPD)</Text>

            <View>
                <TouchableOpacity>Excluir meu cadastro</TouchableOpacity>
                <TouchableOpacity>Download de dados</TouchableOpacity>
                <TouchableOpacity>Trocar senha</TouchableOpacity>
                <TouchableOpacity>Sair</TouchableOpacity>
            </View>
        </ScrollView>
    </View>
  );
}
