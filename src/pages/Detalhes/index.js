import React, {useState} from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';

import styles from './styles';

export default function Detalhes({navigation}) {

  const [num, setNum] = useState(0);

    function incrementa(){
        setNum(num + 1 )
        navigation.navigate('Pronto')
    }
  return (
    <View style={styles.containerA}>
        <View style={styles.containerB}>
          <Text style={{fontSize: 25, marginTop: 10, marginBottom: 20}}>Curso de Gestão de Pessoas</Text>

          <Image style={{width: 320, height: 130}} source={require('./Image/Frame.png')}/>
        </View>
        
          <Text style={{fontSize: 22, marginBottom: 20}}>Detalhes do Curso:</Text>

          <Text style={styles.texto}>A gestão de pessoas no contexto atual</Text>

          <Text style={styles.texto}>Recrutamento e seleção de pessoas</Text>

          <Text style={styles.texto}>Desenvolvimento de equipes</Text>
          
          <Text style={styles.texto}>Liderança e motivação</Text>

          <Text style={styles.texto}>Gestão de conflito em equipe</Text>

          <TouchableOpacity style={styles.botao}>
              <Text styles={styles.textobotao}>Sobre</Text>
          </TouchableOpacity>

          <Text>{num}/50 Membros</Text>

          <TouchableOpacity style={styles.botaoParticipar} onPress={incrementa}>
              <Text>Participar</Text>
          </TouchableOpacity>
    </View>
  );
}
