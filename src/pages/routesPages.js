import React from 'react';
import {createStackNavigator} from '@react-navigation/stack'
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs'

const Stack = createStackNavigator();
const Bottom = createBottomTabNavigator();

import Cursos from './Cursos'
import Home from './Home'
import Palestras from './Palestras'
import RotaDetalhe from './Detalhes'
import Pronto from './Detalhes/Pronto'

function bottomFun(){
    return (
      <Bottom.Navigator>
        <Bottom.Screen name="Home" component={Home} />
        <Bottom.Screen name="Cursos" component={Cursos} />
        <Bottom.Screen name="Palestras" component={Palestras} />
      </Bottom.Navigator>
  );
}


export default function pages() {
  return (
    <Stack.Navigator initialRouteName="Home" screenOptions={{headerTitleAlign: "center", headerTitle: "NOME DO APP"}}>
      <Stack.Screen name="Home" component={bottomFun} />
      <Stack.Screen name="Detalhe" component={RotaDetalhe} />
      <Stack.Screen name="Pronto" component={Pronto} />
    </Stack.Navigator>
  );
}
