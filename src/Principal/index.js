import React from 'react';
import { View, Image, Text, TouchableOpacity, TextInput } from 'react-native';

import styles from './styles';

export default function Principal({ navigation }) {
  // Rotas Para as Pages
  function Entrar(){
    navigation.navigate('Login');
  }
  function Cadastro(){
    navigation.navigate('Cadastro');
  }
  function RoutesHome(){
    navigation.navigate('AcessNull')
  }

  return (
      <View style={styles.container}>
          {/* LOGO */}
          <Image style={{width: 120, height: 120, marginTop: 100}} source={require('./Image/do-utilizador.png')}/>

          <View style={styles.containerEC}>
            <TouchableOpacity style={styles.botao} onPress={Entrar}>
              <Text style={styles.texto}>ENTRAR</Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.botao} onPress={Cadastro}>
              <Text style={styles.texto}>CADASTRAR</Text>
              </TouchableOpacity>
          </View>

          <TouchableOpacity style={styles.botao} onPress={RoutesHome}>
            <Text style={styles.texto}>ACESSAR SEM CONTA</Text>
          </TouchableOpacity>

          <TouchableOpacity><Text style={styles.LinkSuporte}>Central de Ajuda</Text></TouchableOpacity>
          <TouchableOpacity><Text style={styles.LinkSuporte}>Suporte - Perguntas Frequentes</Text></TouchableOpacity>
      </View>
  );  
}
