import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    containerA: {
    },
    containerB: {
        backgroundColor: "#fff",
        marginTop: 50,
        height: 60,
        justifyContent: "center",
        alignItems: "center",
    },
    containerC: {
        width: "100%",
        marginTop: 30,
        alignItems: "center",
        backgroundColor: "#fff"
    },
    textoA: {
        fontSize: 18,
    },
    botao: {
        paddingVertical: 12,
    },
    imagens: {
        height: 160, 
        width: 360, 
        borderRadius: 15,
    },

});