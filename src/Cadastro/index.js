import React from 'react';
import { View, Image, Text, TextInput, TouchableOpacity, KeyboardAvoidingView } from 'react-native';

import styles from './styles';

export default function Cadastro() {
  return (
    <KeyboardAvoidingView style={styles.containerA}>
        <TouchableOpacity>
            <Image source={require('./Image/do-utilizador.png')} style={{width: 120, height: 120}} />
        </TouchableOpacity>
        
        <View style={styles.container}>
          <View style={styles.containerB}>
              <Text style={styles.texto}>Primeiro Nome:</Text>
              <Text style={styles.texto}>Último Nome:</Text>
          </View>
          
          <View  style={styles.containerC}>
            <TextInput style={styles.campoA} placeholder="Primeiro Nome"/>
            <TextInput style={styles.campoA} placeholder="Último Nome"/>
          </View>
        </View>

        <Text style={styles.texto}>E-mail:</Text>
        <TextInput style={styles.campoB} keyboardType="email-address" placeholder="E-mail"/>

        <Text style={styles.texto}>Senha:</Text>
        <TextInput style={styles.campoB} maxLength={6} secureTextEntry placeholder="Password"/>

        <Text style={styles.text}>*A SENHA DEVE TER NO MÍNIMO 6 CARACTERES*</Text>

        <TouchableOpacity style={styles.botao}>
            <Text style={styles.texto}>CADASTRAR</Text>
        </TouchableOpacity>
    </KeyboardAvoidingView>
  );
}
