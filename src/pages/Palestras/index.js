import React from 'react';
import { View, TouchableOpacity, Text, ScrollView, Image } from 'react-native';

import ImgCurso from './image/Frame.png';

import styles from './styles';

export default function Palestras() {

  return (
    <View style={styles.containerA}>
      <ScrollView >
        
        <View style={styles.containerB}>
          <Text style={styles.textoA}>Palestras</Text>
        </View>
      
        <View style={styles.containerC}>
          <TouchableOpacity style={styles.botao}>
            <Image style={styles.imagens} source={ImgCurso} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.botao}>
            <Image style={styles.imagens} source={ImgCurso} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.botao}>
            <Image style={styles.imagens} source={ImgCurso} />
          </TouchableOpacity>
        </View>

      </ScrollView>
    </View>
  );
}
