import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: "#FFF",
        alignItems: "center",
    },
    containerEC:{
        display: "flex",
        flexDirection: "row",
        width: "100%",
        justifyContent: "space-evenly",
        marginTop: 50,
        paddingLeft: 20,
        paddingRight: 20,
    },
    texto:{
        fontSize: 15,
        color: "#333"
    },
    botao:{
        alignItems: "center",
        textAlign: "center",
        width: "50%",
        padding: 10,
        borderWidth: 2,
        borderColor: "#DDD",
        margin: 20,
    },
    LinkSuporte:{
        color: "#e60000"
    }

});