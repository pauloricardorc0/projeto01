import React from 'react';
import { View, TouchableOpacity, Text, ScrollView, Image } from 'react-native';

import ImgCurso from './Image/Frame.png';

import styles from './styles';

export default function Cursos({navigation}) {

  function rotaDetalhe(){
    navigation.navigate('Detalhe');
  }

  return (
    <View style={styles.containerA}>
      <ScrollView >
        
        <View style={styles.containerB}>
          <Text style={styles.textoA}>Cursos</Text>
        </View>
      
        <View style={styles.containerC}>
          <TouchableOpacity style={styles.botao} onPress={rotaDetalhe}>
            <Image style={styles.imagens} source={ImgCurso} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.botao} onPress={rotaDetalhe}>
            <Image style={styles.imagens} source={ImgCurso} />
          </TouchableOpacity>

          <TouchableOpacity style={styles.botao} onPress={rotaDetalhe}>
            <Image style={styles.imagens} source={ImgCurso} />
          </TouchableOpacity>
        </View>

      </ScrollView>
    </View>
  );
}
