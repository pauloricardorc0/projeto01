import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    containerA: {
        flex: 1,
        alignItems: "center",        
    },
    containerB: {
        width: "100%",
        height: 200,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 50
    },
    texto: {
        fontSize: 16,
        marginTop: 5,
    },
    botao: {
        marginTop: 12,
        alignItems: "center",
        width: "100%",
        height: 20,
    }
});