import React from 'react';
import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator();

import Principal from './Principal';
import Login from './Login';
import Cadastro from './Cadastro';
import RoutesPages from './pages/routesPages';

export default function Routes() {
  return (
      <Stack.Navigator initialRouteName="Home" headerMode={"none"}>
          <Stack.Screen name="Home" component={Principal} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Cadastro" component={Cadastro} />
          <Stack.Screen name="AcessNull" component={RoutesPages} />
      </Stack.Navigator>
  );
}
