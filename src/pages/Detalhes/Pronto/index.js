import React from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView } from 'react-native';

import styles from './styles';
import Imgqrcode from './image/qrcodep.png'

export default function Pronto({navigation}) {
  function Voltar() {
    navigation.navigate('Home')
  }
  return (
    <View style={styles.containerA}>
        <ScrollView>
            <View style={styles.containerB}>
            <Text style={{fontSize: 25, marginTop: 10, marginBottom: 20}}>Curso de Gestão de Pessoas</Text>

            <Image style={{width: 320, height: 130, marginBottom: 20}} source={require('../Image/Frame.png')}/>
            </View>
            <Text style={{fontSize: 19}}>Local:</Text>

            <Text style={styles.texto}>R. Dom Pedro II, 2010 - Nossa Senhora das Graças</Text>

            <Text style={styles.texto}>Porto Velho - RO, 76804-116</Text>

            <Text style={{fontSize: 19, marginTop: 12}}>Data:</Text>
            
            <Text style={styles.texto}>Domingo , 26 de Abril de 2020. 08:00H</Text>
            <View style={{width: "100%", alignItems: "center", padding: 10}}>
                <Image source={Imgqrcode} style={{width: 150, height: 150}}/>
            </View>
            
            <TouchableOpacity style={styles.botao} onPress={Voltar}>
                <Text style={{color: "#e60000",fontSize: 15}}>Voltar para cursos</Text>
            </TouchableOpacity>
        </ScrollView>
    </View>
  );
}
