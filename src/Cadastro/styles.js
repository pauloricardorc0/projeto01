import { StyleSheet } from 'react-native'
import { FlingGestureHandler } from 'react-native-gesture-handler';

export default StyleSheet.create({
    containerA:{
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    container: {
        width: "100%",
        justifyContent: "space-around",
        alignItems: "center",
    },
    containerB: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    containerC: {
        width: "100%",
        height: 40,
        alignItems: "center",
        flexDirection: "row",
        justifyContent: "center",
    },
    texto:{
        fontSize: 15,
        color: "#333",
        padding: 3,
    },
    text: {
        paddingVertical: 10,
        fontSize: 10,
    },
    campoA: {
        backgroundColor: "#FFF",
        margin: 10,
        width: "42.5%",
        height: 40,
        textAlign: "center",
        borderWidth: 2,
        borderColor: "#DDD",
        borderRadius: 7,
    },
    campoB: {
        backgroundColor: "#FFF",
        width: "90%",
        height: 40,
        borderWidth: 2,
        borderColor: "#DDD",
        borderRadius: 7,
        textAlign: "center",
    },
    botao: {
        width: "80%",
        height: 50,
        backgroundColor: "#FFF",
        borderWidth: 2,
        borderRadius: 7,
        borderColor: "#DDD",
        alignItems: "center",
        justifyContent: "center",
    }
    
});