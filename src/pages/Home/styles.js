import { StyleSheet } from 'react-native'

export default StyleSheet.create({ 
    containerA: {
        flex: 1,
        justifyContent: "center",
        textAlign: "center",
    },
    containerB: {
        marginTop: 50,
        height: 60,
        justifyContent: "center",
        alignItems: "center"
    },
    textoA: {
        fontSize: 18,
    },
    textoB: {
        marginTop: 20,
        fontSize: 15,
        paddingVertical: 50,
        paddingHorizontal: 30,
        textAlign: "justify",
        
    },

});