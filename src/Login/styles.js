import { StyleSheet } from 'react-native'

export default StyleSheet.create({ 
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    text:{
        fontSize: 15,
        color: "#333",
        textAlign: "center",
        alignItems: "center",
        marginTop: 10,
        marginBottom: 10,
        fontWeight: "500",
    },
    campo: {
        width: "90%",
        borderWidth: 2,
        borderColor: "#DDD",
        borderRadius: 7,
        height: 40,
        backgroundColor: "#FFF",
        textAlign: "center",
    },
    button: {
        width: "80%",
        height: 50,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 2,
        borderRadius: 7,
        borderColor: "#DDD",
        marginTop: 40,
    },

});