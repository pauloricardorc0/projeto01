import React from 'react';
import { View, ScrollView, Text } from 'react-native';

import styles from './styles';

export default function Home() {
  return (
    <View style={styles.containerA}>
      <ScrollView>
        <View style={styles.containerB}>
          <Text style={styles.textoA}>Bem-Vindo(a)</Text>
        </View>

        <Text style={styles.textoB}>Aplicação móvel ou aplicativo móvel, conhecida normalmente por seu nome abreviado app, é um software desenvolvido para ser instalado em um dispositivo eletrônico móvel, como um PDA, telefone celular, smartphone ou um leitor de MP3. Esta aplicação pode ser instalada no dispositivo, ou se o aparelho permitir descarregada pelo usuário através de uma loja on-line, tais como Google Play, App Store ou Windows Phone Store.</Text>
      </ScrollView>
    </View>
  );
}
