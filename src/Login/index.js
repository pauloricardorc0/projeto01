import React from 'react';
import { View, Text, StatusBar, LinearGradient, Image, TouchableOpacity, TextInput, KeyboardAvoidingView } from 'react-native';

import styles from './styles'

export default function Login() {

  return (
    <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <StatusBar hidden={true} />
        <View style={{}}>
            <Image source={require('./Image/ano.png')} style={{width: 150, height: 150}} />
        </View>
        <View style={{width: "100%", alignItems: "center"}}>
            <Text style={styles.text}>E-mail:</Text>

            <TextInput style={styles.campo} placeholder="E-mail"/>

            <Text style={styles.text}>Senha:</Text>

            <TextInput style={styles.campo} placeholder="Password"/>

            <TouchableOpacity style={styles.button}>
                <Text>LOGIN</Text>
            </TouchableOpacity>
        </View>

    </KeyboardAvoidingView>
  );
}
